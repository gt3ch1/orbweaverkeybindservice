So, the way this works is kinda janky, but it ends up working well for my use.

To use this software, do the following:
1) Install and use i3 (at the moment, that's the only WM I got it working with.)
2) Link razer/orbweaver_xkbmap to /usr/share/X11/xkb/symbols/orbweaver
3) From here, you can ran run ./razerKeys <razer/games/[name]> 
  - Or, you can run ./orbweaverLauncher to launch the background program, which 
  will automatically detect what "Game" you are running (some info pulled from 
  games.json, be sure to update that!) and then automatically update your
  orbweaver to your liking!
    - You can install xfce4-notifyd to enable notification bubble support.


-------------
./games.json

In order to use the auto profile changer, you need to have your games.json
similar to the example below.

```
{
    "games": [
        {
            "name": "League of Legends",
            "keybinds": "leagueoflegends"
        },
        {
            "name": "World of Warships",
            "keybinds": "worldofwarships"
        }
    ]
}
```

The "name" selector is matched to the current window - if the "name" appears anywhere
in the title of the selected window, it automatically updates your keypad.

The "keybinds" selector *MUST* match a profile created in razer/games

-------------
./razer/games

This is the directory where you can create your custom orbweaver profiles.

An example of a valid keybind would be as follows:
```
KEY1=1
KEY2=2
KEY3=3
KEY4=4
KEY5=5

KEY6=TAB
KEY7=a
KEY8=s
KEY9=g
KEY10=n

KEY11=o
KEY12=q
KEY13=w
KEY14=e
KEY15=r

KEY16=f
KEY17=d
KEY18=t
KEY19=c
KEY20=v

KEYLALT=x
KEYLEFT=Down
KEYRIGHT=Control_L
KEYUP=
KEYDOWN=
KEYSPACE=Shift_L
```
Do note that you don't have to fill out every key.  If you don't fill out the key,
it will disable the key.
-------------
razer/orbweaver_keymap

This is where the magic of re-keying the orbweaver.  Don't touch.
-------------
./razerKeys.sh

This is where everything above is actually put to use.  This utilizes xinput
to find your orbweaver, and then apply the keymap specified.  It also sets 
the left side buttons on a mambaTE to left/right.
-------------

TODO:
   - Make a pretty GUI.
   - Simplify the code
   - Make other WM's work

