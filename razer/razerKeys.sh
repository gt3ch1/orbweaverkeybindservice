#!/bin/bash 
echo $(pwd)
game=$1
ids=$(xinput  list | grep "Razer Razer Orbweaver Chroma" | sed 's/^.*id=\([0-9]*\)[ \t].*$/\1/')
echo "Binding keymapping for orbweaver..."
for keyboard in $ids
do
	~/scripts/razer/orbweaver_keymap $1
	setxkbmap -device $keyboard orbweaver 2>/dev/null
done
echo "Done..."
echo "Killing all xbindkeys..."
killall xbindkeys
echo "Done..."
echo "Binding mouse keys to functions..."
xbindkeys -f ~/.xbindkeysrc
echo "Done..."
exit 0
