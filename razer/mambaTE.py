#!/usr/bin/env python
import openrazer.client
import random
from time import sleep
devman = openrazer.client.DeviceManager()
devices = devman.devices
kbd = devman.devices[0] 
print(kbd.name)
p=1
canReverse=False
while True:

	for i in range(0,16):

		if (canReverse):
			if(p > 240):
				canReverse = False
				print("Reversing?")	
			kbd.fx.advanced.matrix[0, i] = [p, 0, 0]
			kbd.fx.advanced.draw()

			p+=10
		elif(not canReverse):
			if(p < 20):
				canReverse = True
				print("Anti-Reversing?")	
			kbd.fx.advanced.matrix[0, i] = [p, 0, 0]
			kbd.fx.advanced.draw()

			p-=10
		print("P value: " + str(p))
